class Bus:
    def __init__(self,busid,asientosTotales):
      self.busid=busid
      self.asientosTotales=asientosTotales
      self.asientosDisponibles=asientosTotales
    def getBusid(self):
        return self.busid
    def setBusid(self,busid):
        self.busid=busid
    def getAsientosTotales(self):
        return self.asientosTotales
    def setAsientosTotales(self,asientosTotales):
        self.asientosTotales=asientosTotales
    def getAsientosDisponibles(self):
        return self.asientosDisponibles
    def setAsientosDisponibles(self,asientosDisponibles):
        self.asientosDisponibles=asientosDisponibles

    def vender_tickets(self,tickets):
        if tickets <=self.asientosTotales:
            asientosDisponibles = int(self.getAsientosDisponibles()) - tickets
            self.setAsientosDisponibles(asientosDisponibles)
            self.setAsientosDisponibles(self.asientosDisponibles)
            resultado = 0
        else:
            resultado = 1
        return resultado

    def devolucion_tickets(self,tickets):
        asientosOcupados=self.getAsientosTotales()-self.getAsientosDisponibles()
        if tickets> asientosOcupados:
            resultado=0
        else:
            #setAsientosDisponibles(self.getAsientosDisponibles()+tickets)
            asientosDisponibles=int(self.getAsientosDisponibles())+tickets
            self.setAsientosDisponibles(asientosDisponibles)
            self.setAsientosDisponibles(self.asientosDisponibles)
            resultado=1
        return resultado

    def get_bus(self,busid, lista):
        busBuscado=0
        for bus in lista:
            if bus.busid == busid:
                busBuscado = bus
                break
        return busBuscado   
    
    def estado_bus(self,busid,buses):
        bus = self.get_bus(busid, buses)
        asientosDisponibles=self.getAsientosDisponibles()
        asientosTotales=self.getAsientosTotales()
        resultado= f"El bus con el {busid} tiene {asientosTotales} asientos totales  y  {asientosDisponibles} asientos disponibles"  
        return resultado
    #def agregar_bus(bus):
        
        
    def __str__(self):
        return "soy el bus con el id " + str(self.busid)
"""    
buses = []
bus1 = Bus(1, 70)
bus2 = Bus(2, 50)
bus3= Bus(3, 40)
buses.append(bus1)
buses.append(bus2)
buses.append(bus3)
for bus in buses:
    print(bus)


bus3.estado_bus(3, buses)"""
    
  
