from Bus import Bus
class Main:
    def menu():
        print("1- Crear Bus\n2-Seleccionar bus\n3-Venta de billetes\n4-Devolucion"+
          "de billetes\n5-Estado de la venta\n0-Salir del programa")
    
    buses = []
    opcion = -1
    while opcion!=0:
        menu()
        opcion=int(input("Seleccione una opcion "))
        if opcion==1:
            idBus = 1
            plazas = int(input('Cuantas plazas tiene el bus? '))
            bus = Bus(idBus, plazas)
            idBus += 1
            buses.append(bus)
        elif opcion==3:
            tickets=int(input("Cuantos tickets va a comprar"))
            if tickets < 0:
                raise Exception('El numero introducido es negativo y se esperava un positivo')
            resultado = bus.vender_tickets(tickets)
            if resultado==0:
                print("Venta realizada con exito")
            else:
                print("Error, venta no realizada")
        elif opcion==4:
            tickets=int(input("Cuantos tickets quiere devolver"))
            resultado = bus.devolucion_tickets(tickets)
            if resultado==0:
                print("Error, no se ha podido hacer la devolucion")
            else:
                 print("Devolucion realizada con exito")
        elif opcion==5:
            busid=int(input("Que bus quieres ?? "))
            resultado=bus.estado_bus(busid,buses)
            print(resultado)
            
        
    print(buses)    
    
